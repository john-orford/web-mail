# Email App

The aim is to be a simple email client which uses two email services and fallsback when one is not available.

The code is cleanly broken into two parts.

# Backend

The backend provides a REST API which accepts email data to be sent and a status of the send request for the email client.

The backend is based on the Scotty Haskell web framework, which is in effect a collection of Haskell technologies packaged together.

This was chosen because it is popular with the Haskell community and the author wanted to learn it.

The advantages are, very little boiler plate while writing pure Haskell.

The main disadvantage is a lack of documentation, and the necessity to understand howthings work via library unit tests and library source code.

# Frontend

The frontend send and consumes JSON through a REST API.

It is based on the Elm language and archictecture.

Elm was chosen because I am very familiar with it.

The advantages of Elm is stability. Rumtime exceptions are a matter for the builders of the language, not the user of the language - i.e. if your porgram crashes then it is not your problem but the Elm community's, therefore this does not happen very often.

I believe this is a unique selling proposition.

The disadvantages are that all interop between Javascript & Elm is strictly regulated, leading to slower development.

# Purely Statically Typed

Both Elm and Haskell are Pure Statically Typed languages. This means their function types are very accurate and lead to several advantages. The main one, is a mathematical gaurantee
 of well-formed output.
 
This means that once my code compiles and promises to give me JSON or HTML data for example, this is not just a promise of no failures but a guarantee.


# Todo

 * Haskell Quickcheck testing
 * More informative error codes from backend
 * Authorisation endpoint (check hashed password against database etc.)

 * Selenium-style testing
 * Elm randomised fuzz testing
 * Display better looking error messages in front end
 * Front end field validation
 * A list of sent emails (explore database integration using Haskell to store emails)
 
 * Host example

 * More comments
 * A few explainer blog posts for beginner functional programmers