module Main exposing (main)

import Models exposing (model, Model)
import View exposing (view)
import Msgs exposing (Msg)
import Update exposing (update)
import Routing exposing (parseLocation)
import Navigation exposing (Location)


main : Program Never Model Msg
main =
    Navigation.program
        Msgs.OnLocationChange
        { view = view
        , update = update
        , init = init
        , subscriptions = subscriptions
        }


{-| init model with current location from address bar
-}
init : Location -> ( Model, Cmd msg )
init location =
    let
        currentRoute =
            Routing.parseLocation location
    in
        ( model currentRoute, Cmd.none )


{-| no subs required right now
-}
subscriptions : a -> Sub msg
subscriptions model =
    Sub.none
