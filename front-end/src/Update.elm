module Update exposing (update)

import Models exposing (Model, EmailData, emailStatusDecoder)
import Routing exposing (Route)
import Msgs exposing (Msg(..))
import Http exposing (post, send, jsonBody, Error(BadUrl, BadPayload, NetworkError, BadStatus, Timeout))
import Json.Encode exposing (encode, string, object)
import Routing exposing (parseLocation)
import Navigation exposing (newUrl)


update : Msgs.Msg -> Models.Model -> ( Models.Model, Cmd Msgs.Msg )
update msg model =
    case Debug.log "log" msg of
        To to ->
            ( { model | to = to }, Cmd.none )

        Subject subject ->
            ( { model | subject = subject }, Cmd.none )

        Content content ->
            ( { model | content = content }, Cmd.none )

        Login ->
            ( model
            , newUrl "/#compose"
            )

        Send ->
            ( model
            , sendEmailRequest { to = model.to, subject = model.subject, content = model.content }
            )

        ShowCompose ->
            ( model
            , newUrl "/#compose"
            )

        ShowSent ->
            ( model
            , newUrl "/#sent"
            )

        OnLocationChange location ->
            let
                newRoute =
                    parseLocation location
            in
                ( { model | route = newRoute }, Cmd.none )

        SendEmailRequest (Ok response) ->
            ( { model
                | status = response.status
              }
            , Cmd.none
            )

        SendEmailRequest (Err error) ->
            case error of
                BadUrl error ->
                    ( { model | status = error }, Cmd.none )

                Timeout ->
                    ( { model | status = "Timeout" }, Cmd.none )

                NetworkError ->
                    ( { model | status = "Networking Error" }, Cmd.none )

                BadStatus p ->
                    ( { model | status = p.status.message }, Cmd.none )

                BadPayload s p ->
                    ( { model | status = p.status.message }, Cmd.none )


encodeEmail : { a | to : String, subject : String, content : String } -> Http.Body
encodeEmail a =
    jsonBody
        (Json.Encode.object
            [ ( "to", Json.Encode.string a.to )
            , ( "subject", Json.Encode.string a.subject )
            , ( "content", Json.Encode.string a.content )
            ]
        )


sendEmailRequest : EmailData -> Cmd Msgs.Msg
sendEmailRequest a =
    let
        url =
            "http://localhost:3000/send"
    in
        Http.post url (encodeEmail a) emailStatusDecoder
            |> Http.send SendEmailRequest
