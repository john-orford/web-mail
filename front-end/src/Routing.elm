module Routing exposing (..)

import Navigation exposing (Location)
import UrlParser exposing (top, s, map, oneOf, Parser, parseHash)


type Route
    = LoginRoute
    | ComposeRoute
    | SentRoute
    | NotFoundRoute


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map LoginRoute UrlParser.top
        , map ComposeRoute (UrlParser.s "compose")
        , map SentRoute (UrlParser.s "sent")
        ]


parseLocation : Location -> Route
parseLocation location =
    case (parseHash matchers location) of
        Just route ->
            route

        Nothing ->
            NotFoundRoute
