module View exposing (view)

import Msgs exposing (Msg)
import Models exposing (model, Model)
import Routing exposing (Route)
import LoginView exposing (loginView)
import Compose exposing (view)
import Sent exposing (view)
import Html exposing (Html, div, text)
import Html.Attributes exposing (class)


view : Model -> Html Msg
view model =
    div [ class "parent" ]
        [ page model ]


page : Model -> Html Msg
page model =
    case model.route of
        Routing.LoginRoute ->
            loginView model

        Routing.ComposeRoute ->
            Compose.view model

        Routing.SentRoute ->
            Sent.view model

        Routing.NotFoundRoute ->
            notFoundView


notFoundView : Html msg
notFoundView =
    div []
        [ text "Not found" ]
