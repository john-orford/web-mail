module Sent exposing (view)

import Html exposing (node, Html, div, span, img, button, Attribute)
import Html.Attributes exposing (class, style, attribute, classList)
import Msgs exposing (Msg(..))
import Models exposing (Model, model)
import Common exposing (layout)


view : Model -> Html Msg
view model =
    layout model html "Compose Email"


html : Html msg
html =
    div
        [ class "mdc-layout-grid__cell--span-10" ]
        [ Html.text "Sent"
        , div
            [ style [ ( "float", "right" ) ]
            ]
            []
        , div [ style [ ( "margin-top", "4em" ) ] ] []
        ]
