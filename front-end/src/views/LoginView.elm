module LoginView exposing (loginView)

import Msgs exposing (Msg(..))
import Models exposing (model, Model)
import Html exposing (div, text, Html, button, span, input, h2, h1, section)
import Html.Events exposing (onInput, onClick)
import Html.Attributes exposing (class, style, placeholder, type_)


loginView : Model -> Html Msg
loginView model =
    div
        [ class "centerAlign" ]
        [ div
            [ class "mdc-layout-grid"
            , class "max-width"
            ]
            -- left blank space
            [ div
                [ class "mdc-layout-grid__cell--span-2" ]
                []
              -- central login dialogue
            , div
                [ class "mdc-layout-grid__cell--span-7"
                , class "mdc-textfield"
                ]
                [ div
                    [ class "mdc-card" ]
                    [ section
                        [ class "mdc-card__media"
                        , style
                            [ ( "background-image", "url(images/blossom.jpg)" )
                            , ( "background-size", "cover" )
                            , ( "background-repeat", "no-repeat" )
                            , ( "height", "13em" )
                            ]
                        ]
                        []
                    , section
                        [ class "mdc-card__primary" ]
                        [ h1
                            [ class "mdc-card__title--large" ]
                            [ text "Email App" ]
                        , h2
                            [ class "mdc-card__subtitle" ]
                            []
                        ]
                    , section
                        [ class "mdc-card__actions" ]
                        [ div
                            [ style
                                [ ( "width", "100%" )
                                , ( "margin-bottom", "1em" )
                                ]
                            ]
                            [ span
                                [ style
                                    [ ( "margin-right", "3em" )
                                    , ( "margin-left", "1em" )
                                    , ( "margin-top", "0.5em" )
                                    , ( "float", "left" )
                                    ]
                                ]
                                [ input
                                    [ type_ "text"
                                    , placeholder "User"
                                    , class "mdc-textfield__input"
                                    ]
                                    []
                                ]
                            , span
                                [ style
                                    [ ( "float", "left" )
                                    , ( "margin-top", "0.5em" )
                                    ]
                                ]
                                [ input
                                    [ type_ "password"
                                    , placeholder "Password"
                                    , class "mdc-textfield__input"
                                    ]
                                    []
                                ]
                            , span
                                [ style
                                    [ ( "float", "right" )
                                    , ( "margin-right", "1em" )
                                    ]
                                ]
                                [ button
                                    [ class "mdc-button"
                                    , class "mdc-button--raised"
                                    , onClick Login
                                    ]
                                    [ text "Login" ]
                                ]
                            ]
                        ]
                    ]
                ]
              -- right blank space
            , div
                [ class "mdc-layout-grid__cell--span-2" ]
                []
            ]
        ]
