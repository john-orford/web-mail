module Compose exposing (view)

import Html exposing (node, Html, div, span, img, button, Attribute, input, text, textarea, br)
import Html.Attributes exposing (class, style, attribute, classList, placeholder, rows)
import Html.Events exposing (onInput, onClick)
import Msgs exposing (Msg(..))
import Models exposing (Model, model)
import Common exposing (layout)


view : Model -> Html Msg
view model =
    layout model (html model.status) "Compose Email"


html : String -> Html Msg
html status =
    div
        [ class "mdc-layout-grid__cell--span-10" ]
        [ div
            [ class "mdc-textfield mdc-textfield--fullwidth mdc-textfield--upgraded" ]
            [ input [ class "mdc-textfield__input", placeholder "To", onInput To ] []
            ]
        , div
            [ class "mdc-textfield mdc-textfield--fullwidth mdc-textfield--upgraded" ]
            [ input [ class "mdc-textfield__input", placeholder "Subject", onInput Subject ] []
            ]
        , div
            [ class "mdc-textfield mdc-textfield--fullwidth mdc-textfield--upgraded mdc-textfield--multiline"
            ]
            [ textarea [ class "mdc-textfield__input", placeholder "Content", rows 8, onInput Content ] []
            ]
        , br [] []
        , button [ class "mdc-button mdc-button--raised", onClick Send ] [ text "Send" ]
        , br [] []
        , br [] []
        , br [] []
        , div []
            [ span [] [ text "Status: " ]
            , span [] [ text status ]
            ]
        ]
