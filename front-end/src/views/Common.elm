module Common exposing (layout, hrMdl, divButton)

{-| Reusable html snippets
-}

import Html exposing (div, span, text, Html, hr, img, h1, h2, h3, section, header, li, ul, node)
import Html.Attributes exposing (style, class, attribute, classList)
import Msgs exposing (Msg(..))
import Html.Events exposing (onClick)
import Models exposing (model, Model)


hrMdl : Html msg
hrMdl =
    hr [ class "mdc-list-divider mdc-list-divider--inset" ] []


layout : Model -> Html Msg -> String -> Html Msg
layout model grid title =
    div
        [ style
            [ ( "display", "flex" )
            , ( "flex-direction", "column" )
            , ( "justify-content", "center" )
            ]
        , class "mdc-typography"
        ]
        [ --top
          head
            [ span
                [ class "mdc-toolbar__title" ]
                [ text title ]
            ]
            []
          -- left menu
        , div
            [ class "mdc-layout-grid"
            , style [ ( "width", "100%" ) ]
            ]
            [ div
                [ class "mdc-layout-grid__cell--span-2"
                ]
                [ div
                    [ class "mdc-list-group" ]
                    [ spacer
                    , listItem editIcon "Compose" ShowCompose
                    , spacer
                    , listItem historyIcon "Sent" ShowSent
                    ]
                ]
              -- grid etc.
            , grid
            ]
        ]


spacer : Html msg
spacer =
    div [ style [ ( "height", "0.01em" ) ] ] []


listItem : String -> String -> msg -> Html msg
listItem i t a =
    li
        [ classList
            [ ( "link_light", True )
            , ( "mdc-list-item", True )
            , ( "mdc-ripple-surface", True )
            ]
        , onClick a
        ]
        [ img
            [ class "mdc-list-item__start-detail"
            , attribute "src" i
            ]
            []
        , text t
        ]


head : List (Html msg) -> List (Html msg) -> Html msg
head left right =
    header
        [ classList
            [ ( "mdc-toolbar", True )
            , ( "mdc-theme--dark", True )
            ]
        ]
        [ section
            [ classList
                [ ( "mdc-toolbar__section", True ), ( "mdc-toolbar__section--align-start", True ) ]
            ]
            left
        , section
            [ class "section--align-end" ]
            right
        ]


menuItem : String -> String -> Html msg
menuItem i t =
    div []
        [ img
            [ style [ ( "float", "left" ), ( "margin-right", "1em" ) ]
            , attribute "src" i
            ]
            []
        , h3
            [ class "mdc-list-group__subheader" ]
            [ text t ]
        ]


divButton : String -> String -> Html msg
divButton t i =
    div
        [ classList
            [ ( "mdc-button", True )
            , ( "mdc-button--compact", True )
            ]
        ]
        [ img [ attribute "src" i, style [ ( "float", "left" ), ( "margin-right", "0.25em" ), ( "margin-top", "0.5em" ) ] ] []
        , span [] [ text t ]
        ]


historyIcon : String
historyIcon =
    "/images/material-design-icons-master/ic_history_black_24dp_1x.png"


editIcon : String
editIcon =
    "/images/material-design-icons-master/editor/svg/production/ic_mode_edit_24px.svg"


exitIcon : String
exitIcon =
    "/images/material-design-icons-master/action/2x_web/ic_exit_to_app_white_18dp.png"
