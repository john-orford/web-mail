module Msgs exposing (Msg(..))

import Navigation exposing (Location)
import Http exposing (Error)
import Models exposing (EmailStatusData)


type Msg
    = Login
    | OnLocationChange Location
    | ShowCompose
    | ShowSent
    | Content String
    | To String
    | Subject String
    | SendEmailRequest (Result Http.Error EmailStatusData)
    | Send
