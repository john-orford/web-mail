module Models exposing (model, Model, emailStatusDecoder, EmailStatusData, EmailData)

import Json.Decode exposing (map, Decoder, string, field, int, list, bool, null, oneOf)
import Routing exposing (Route(..))


type alias Model =
    { route : Route
    , to : String
    , subject : String
    , content : String
    , status : String
    }


model : Route -> Model
model route =
    { route = route
    , to = ""
    , subject = ""
    , content = ""
    , status = ""
    }


emailStatusDecoder : Decoder EmailStatusData
emailStatusDecoder =
    Json.Decode.map EmailStatusData (field "status" Json.Decode.string)


type alias EmailData =
    { to : String, content : String, subject : String }


type alias EmailStatusData =
    { status : String }
