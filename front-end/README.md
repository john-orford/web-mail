

# Front End

Download required packages:

`elm-package install`

Compile & start with:

`elm-live Main.elm --output=app.js --open --debug --host=localhost`

