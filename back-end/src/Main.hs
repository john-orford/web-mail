{-# LANGUAGE OverloadedStrings #-}

module Main where

import Web.Scotty (ScottyM(..), post, ActionM(..), json, jsonData, scotty, middleware, body)
import Network.Wai.Middleware.Cors (cors, CorsResourcePolicy(..), simpleMethods, simpleHeaders, corsOrigins, corsMethods, corsRequestHeaders, corsExposedHeaders, corsMaxAge, corsVaryOrigin, corsIgnoreFailures, corsRequireOrigin)
import SendEmail (sendEmail, Email(..))
import Control.Monad.IO.Class (liftIO)
import Data.Aeson (object)
import Data.Aeson.Types (Value(..),(.=))
import Data.Text (Text(..))

simpleCors' = cors $ const $ Just simpleCorsResourcePolicy'

simpleCorsResourcePolicy' :: CorsResourcePolicy
simpleCorsResourcePolicy' = CorsResourcePolicy
    { corsOrigins = Nothing
    , corsMethods = simpleMethods
    , corsRequestHeaders = simpleHeaders
    , corsExposedHeaders = Nothing
    , corsMaxAge = Nothing
    , corsVaryOrigin = False
    , corsRequireOrigin = False
    , corsIgnoreFailures = False
    }

main :: IO ()
main = do
    putStrLn "Starting Server..."
    scotty 3000 $ do
        middleware simpleCors'
        routes

routes :: ScottyM ()
routes = 
    post "/send" $ do
        -- parse json from post request
        j <- jsonData :: ActionM Email
        -- send json to sendMail function
        r <- liftIO $ sendEmail j
        body >>= liftIO . print
        json $ case r of
            --best to return more descriptive info here...
            Left _ ->
                emailStatus "KO"
            Right _ ->
                emailStatus "OK"

emailStatus :: Text -> Value
emailStatus s = object
    [
    "status" .= String s 
    ]
