-- Send emails to Mailgun & SendGrid APIs

{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}
{-# OPTIONS_GHC -fno-warn-unused-binds #-}

module SendEmail where

import Control.Lens ((&), (^.), (.~), (?~))
import Data.Aeson (object, FromJSON, ToJSON)
import GHC.Generics (Generic)
import Data.Aeson.Types (Value(..), toJSON, (.=))
import Control.Exception (throwIO)
import Network.Wreq (Response(..), responseStatus, statusCode, statusMessage, checkResponse, responseBody, auth, Part(..), postWith, header, defaults, asValue, basicAuth, partText)
import Network.Wreq.Types ( Options, ResponseChecker(..) )
import Network.HTTP.Client (HttpException(..), HttpExceptionContent(..))
import Control.Monad (void)
import Data.ByteString (ByteString(..))
import Data.ByteString.Lazy (ByteString(..))
import Data.Text (Text(..))
import Data.String (IsString(..))
import System.Environment (getEnv)
import Data.String.Conversions (cs)

-----------------------------------------------------------
--Mailgun related

mgFormData :: Email -> [Part]
mgFormData (Email to subject content) = [partText "from" "Mailgun Sandbox <postmaster@sandboxf7cfe5a2b4db4712b94d183d54cb42d7.mailgun.org>", 
      partText "to" to, 
      partText "subject" subject, 
      partText "text" content
      ]

mgOpts :: IO Options
mgOpts = do
  --take from BASH environment variable
  k <- getEnv "MGKEY"
  return $ defaults
    & checkResponse .~ Just customResponseCheck
    & auth ?~ basicAuth "api" (cs k)

mgSend :: Email -> IO (Response Value)
mgSend email = do
  os <- mgOpts
  v <- postWith os "https://api.mailgun.net/v3/sandboxf7cfe5a2b4db4712b94d183d54cb42d7.mailgun.org/messages" (mgFormData email)
  asValue v

-----------------------------------------------------------
--SendGrid related

sgData :: Email -> Value
sgData (Email to subject content) = object
  [ "personalizations" .= 
    [ object 
      [ "to" .= 
        [ object 
          [ "email" .= String to ]
        ]
      ]
    ],
    "from" .= object [ "email" .= String "john.orford@gmail.com" ],
    "subject" .= String subject,
    "content" .= [ object ["type" .= String "plain/text", "value" .= String content]]
  ]

sgOpts :: IO Options
sgOpts = do
  --take from BASH environment variable
  k <- getEnv "SGKEY"
  return $ defaults 
    & checkResponse .~ Just customResponseCheck
    & header "Accept" .~ ["application/json"] 
    & header "Authorization" .~ [cs k] 
  
sgSend :: Email -> IO (Response Data.ByteString.Lazy.ByteString)
sgSend email = do
  os <- sgOpts
  postWith os "https://api.sendgrid.com/v3/mail/send" $ toJSON (sgData email)

-----------------------------------------------------------
-- Start here

data Email = Email { to :: Text, subject :: Text, content :: Text } deriving (Generic)

instance ToJSON Email
instance FromJSON Email

sendEmail :: Email -> IO (Either Data.ByteString.ByteString String)
sendEmail email = do
  -- first shot at sending email
  e <- mgSend email
  let r = parseResponse e
  case r of
    --succcess case
    Right a ->
      return r
    --every other case
    _ ->
      -- backup send email
      do
        e' <- sgSend email
        let r' = parseResponse e'
        return r'


parseResponse :: IsString b => Response body -> Either Data.ByteString.ByteString b
parseResponse r =
  case r ^. responseStatus . statusMessage of
    --send grid
    "OK"        -> Right "OK"
    --mail gun
    "Accepted"  -> Right "OK"
    --more parsing for better error messages...
    a           -> Left "KO"
        
customResponseCheck :: ResponseChecker
customResponseCheck request response = 
  case response ^. responseStatus . statusCode of
    --check whether integer, instead? catch more statuses?
    200 -> return ()
    202 -> return ()
    404 -> return ()
    400 -> return ()
    -- throw error, if no parseable status is found in response
    _   -> throwIO $ HttpExceptionRequest request (StatusCodeException  (void response) "")