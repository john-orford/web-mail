# Back-End

Save the environment variable for Mailgun and Send Grid API keys

`export SGKEY="Bearer 101010101010"`

`export MGKEY="key-1010101010"`

Start Haskell's interactive shell,

`stack ghci`

and start the Scotty server

`main`

Send a test email,

`curl -XPOST -H 'Content-Type:application/json' -d '{"to":"john.orford@gmail.com","subject":"1234 ertgrg","content":"sdfsdf"}' http://localhost:3000/send`

Receive a status, e.g.

`{"status":"OK"}`

